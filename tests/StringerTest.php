<?php

namespace Vicimus\Stringer;

/**
 * Test the basic functionality of the Stringer library
 *
 * @author Rob Houston
 */
class StringerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Mock a fake cookie
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $_COOKIE['customerID'] = sha1(rand());
    }

    /**
     * Test instantiation
     *
     * @return void
     */
    public function testConstructor()
    {
        $stringer = new Stringer;

        $this->assertInstanceOf(Stringer::class, $stringer);
    }

    /**
     * Test getting a customer ID from the cookie
     *
     * @return void
     */
    public function testGetCustomerID()
    {
        $stringer = new Stringer;

        $customerID = $stringer->getCustomer();

        $this->assertTrue(strlen($customerID) > 0);
        $this->assertInternalType('string', $customerID);
    }

    /**
     * Test getting a Customer ID when the cookie is not set
     *
     * @return void
     */
    public function testGetCustomerIDFailure()
    {
        unset($_COOKIE['customerID']);
        $stringer = new Stringer;

        $customerID = $stringer->getCustomer();

        $this->assertNull($customerID);
    }

    /**
     * Ensure that a snippet can be generated property
     *
     * @return void
     */
    public function testGetSnippet()
    {
        $stringer = new Stringer(openssl_random_pseudo_bytes(32), 57);
        $snippet = $stringer->snippet(58, [
            'www.oakridgeford.com',
        ]);

        $this->assertInternalType('string', $snippet);
        $this->assertContains('=58', $snippet);
    }

    /**
     * Ensure that a snippet can be generated property
     *
     * @return void
     */
    public function testGetSnippetWithoutArgs()
    {
        $stringer = new Stringer(
            openssl_random_pseudo_bytes(32),
            57,
            ['www.oakridgeford.com']
        );

        $snippet = $stringer->snippet();

        $this->assertInternalType('string', $snippet);
        $this->assertContains('=57', $snippet);
    }

    /**
     * Ensure that no crashes happen
     *
     * @return void
     */
    public function testGetSnippedWithoutInfo()
    {
        $stringer = new Stringer(null, null, [null]);

        $snippet = $stringer->snippet();

        $this->assertInternalType('string', $snippet);
        $this->assertContains('No idsite', $snippet);
    }
}

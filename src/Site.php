<?php

namespace Vicimus\Stringer;

/**
 * Represents a Stringer site to be tracked
 *
 * @author Jordan
 */
class Site
{
    /**
     * The URLs used with the tracking code
     *
     * @var array
     */
    protected $urls = [];

    /**
     * The ID of the site
     *
     * @var integer
     */
    protected $id;

    /**
     * The Odometer client ID
     *
     * @var string
     */
    protected $cid;

    /**
     * Get a protected property
     *
     * @param string $property The property to get
     *
     * @return mixed
     */
    public function __get($property)
    {
        if ($property === 'urls') {
            return $this->formatURLS();
        }

        return $this->$property;
    }

    /**
     * Construct a new site with the ID and urls to be used
     *
     * @param int    $id   The ID of the site
     * @param array  $urls The URLS to include in the tracker
     * @param string $cid  The Odometer Client ID
     */
    public function __construct($id, array $urls, $cid)
    {
        $this->urls = $urls;
        $this->id = $id;
        $this->cid = $cid;
    }

    /**
     * Format the URLs for output
     *
     * @return string
     */
    public function formatURLS()
    {
        $urls = $this->urls;
        foreach ($urls as &$url) {
            $url = '"'.$url.'"';
        }

        $joined = implode(', ', $urls);
        return $joined;
    }
}

<?php

namespace Vicimus\Stringer;

/**
 * Class to collect information from the stringer API.
 */
class Stringer
{
    /**
     * The index in the cookies where the ID is stored
     *
     * @var string
     */
    const KEY = 'customerID';

    /**
     * The Client ID of the consumer using this class
     *
     * @var string
     */
    protected $cid = null;

    /**
     * The site ID
     *
     * @var int
     */
    protected $sid = null;

    /**
     * The URls to track
     *
     * @var string[]
     */
    protected $urls = [];

    /**
     * Provide a client id to the constructor to access certain functions
     *
     * @param string  $client Your Odometer Client ID
     * @param integer $sid    Your site's ID
     * @param array   $urls   The URLs to track
     */
    public function __construct($client = null, $sid = null, array $urls = array())
    {
        $this->cid = $client;
        $this->sid = $sid;
        $this->urls = $urls;
    }

    /**
     * Retrieve the customer id set in the cookie.
     *
     * @return string The ID of the customer
     */
    public function getCustomer()
    {
        return $this->hasID() ? $_COOKIE[self::KEY] : null;
    }

    /**
     * Check if the cookie has been set
     *
     * @return bool
     */
    public function hasID()
    {
        return array_key_exists(self::KEY, $_COOKIE);
    }

    /**
     * Generate the site snippet
     *
     * @param integer $idsite The ID of the site to be tracked
     * @param array   $urls   The urls to include in the tracking code
     *
     * @return string
     */
    public function snippet($idsite = null, array $urls = array())
    {
        $code = '';
        if (!$idsite) {
            $idsite = $this->sid;
        }

        if (!count($urls)) {
            $urls = $this->urls;
        }

        if (!$idsite) {
            return '<!-- No idsite -->';
        }

        if (!count($urls) || !$urls[0]) {
            return '<!-- No URLs -->';
        }

        $site  = new Site($idsite, $urls, $this->cid);

        ob_start();
        require __DIR__.'/views/snippet.php';
        $code = ob_get_contents();
        ob_end_clean();
        
        return $code;
    }
}

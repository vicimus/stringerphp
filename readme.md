#StringerPHP#

StringerPHP is a lightweight library used to get information from Stringer.

Stringer is part of the Odometer ecosystem, specifically handling visitor tracking.

The main purpose of this library is to provide an easy interface for retrieving
information about the current visitor.

## Installation #

`composer require vicimus/stringerphp ^1.0`

## Getting a Visitor ID #

```
$stringer = new Vicimus\StringerPHP\Stringer;
$userID = $stringer->getCustomer();
```